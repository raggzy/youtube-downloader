package com.bistrashkin;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RetrierTest {
    @Test
    public void testSimple() throws Exception {
        Retrier retrier = new Retrier(3);
        AtomicInteger timeExecuted = new AtomicInteger();
        retrier.run(timeExecuted::incrementAndGet);
        assertEquals(timeExecuted.intValue(), 1);
    }

    @Test
    public void testAllFailAndLastExceptionThrown() throws Exception {
        Retrier retrier = new Retrier(3);
        AtomicInteger timeExecuted = new AtomicInteger();
        InvalidSelection expectedException = new InvalidSelection();
        try {
            retrier.run(() -> {
                try {
                    throw timeExecuted.intValue() == 2 ? expectedException : new RuntimeException();
                } finally {
                    timeExecuted.incrementAndGet();
                }
            });
            fail("Should be exception");
        } catch (Exception e) {
            assertEquals(timeExecuted.intValue(), 3);
            assertEquals(expectedException, e);
        }
    }

    @Test
    public void testMatchedFail() {
        Retrier retrier = new Retrier(3, InvalidSelection.class);
        AtomicInteger timeExecuted = new AtomicInteger();
        try {
            retrier.run(() -> {
                try {
                    throw new InvalidSelection();
                } finally {
                    timeExecuted.incrementAndGet();
                }
            });
            fail("Should be exception");
        } catch (Exception e) {
            assertEquals(3, timeExecuted.intValue());
        }
    }

    @Test
    public void testNotMatchedFail() {
        Retrier retrier = new Retrier(3, InvalidSelection.class);
        AtomicInteger timeExecuted = new AtomicInteger();
        try {
            retrier.run(() -> {
                try {
                    throw new RuntimeException();
                } finally {
                    timeExecuted.incrementAndGet();
                }
            });
            fail("Should be exception");
        } catch (Exception e) {
            assertEquals(1, timeExecuted.intValue());
        }
    }
}
