package com.bistrashkin;

import org.apache.commons.cli.CommandLine;

/**
 * Created by bogdanis on 11/22/2016.
 */
public class MainOptions {
    public static final String FFMPEG_PATH_DEFAULT = "C:\\Users\\raggzy\\Programs\\ffmpeg-4.1-win64-static\\bin\\ffmpeg.exe";

    private String playlistUrl;
    private String ffmpegPath;
    private String proxyHost;
    private static final String TMP_FOLDER_DEFAULT = "C:\\Temp";
    private int proxyPort;

    public MainOptions() {
    }
    private String destPath;
    private String tmpFolder;

    public void parse(CommandLine commandLine) {
        this.playlistUrl = commandLine.getOptionValue("playlistUrl");
        this.destPath = commandLine.getOptionValue("destPath");
        this.ffmpegPath = commandLine.getOptionValue("ffmpegPath", FFMPEG_PATH_DEFAULT);
        this.proxyHost = commandLine.getOptionValue("proxyHost");
        String proxyPort = commandLine.getOptionValue("proxyPort");
        this.proxyPort = proxyPort == null ? -1 : Integer.valueOf(proxyPort);
        this.tmpFolder = commandLine.getOptionValue("tmpFolder", TMP_FOLDER_DEFAULT);
    }

    public void validate() {
        if (playlistUrl == null || destPath == null || tmpFolder == null) {
            throw new IllegalArgumentException("URL or dest or tmpFolder are empty");
        }

        if (proxyHost != null && proxyPort < -1) {
            throw new IllegalArgumentException("invlaid proxyPort");
        }
    }

    public String getPlaylistUrl() {
        return playlistUrl;
    }

    public void setPlaylistUrl(String playlistUrl) {
        this.playlistUrl = playlistUrl;
    }

    public String getDestPath() {
        return destPath;
    }

    public void setDestPath(String destPath) {
        this.destPath = destPath;
    }

    public String getFfmpegPath() {
        return ffmpegPath;
    }

    public void setFfmpegPath(String ffmpegPath) {
        this.ffmpegPath = ffmpegPath;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getTmpFolder() {
        return tmpFolder;
    }
}
