package com.bistrashkin;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

public class FtpFileSystemConnection implements TargetFileSystemConnection {
    private String initialPath;
    private String extractPath;
    private FTPClient ftpClient;

    public FtpFileSystemConnection(String path) {
        this.initialPath = path;
    }

    @Override
    public void open() throws IOException {
        URL url = new URL(initialPath);
        ftpClient = new FTPClient();
        ftpClient.setAutodetectUTF8(true);
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.connect(url.getHost(), url.getPort());
        String[] split = url.getUserInfo().split(":");
        String username = split[0];
        String password = split[1];
        if (!ftpClient.login(username, password)) {
            throw new IOException("Unable to login");
        }
        if (!ftpClient.hasFeature("UTF8")) {
            throw new IOException("Not supported UTF-8. Fuck it");
        }
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
        ftpClient.enterLocalPassiveMode();
        extractPath = String.format("ftp://%s:%s@%s:%d", username, password, url.getHost(), url.getPort());
    }

    @Override
    public void close() throws IOException {
        ftpClient.disconnect();
    }

    public static void main(String[] args) throws IOException {
        try (FtpFileSystemConnection conn = new FtpFileSystemConnection("ftp://192.168.0.102:2221/music/RAM Records")) {
            conn.open();
            System.out.println(conn.dirExists("/music"));
            System.out.println(Arrays.toString(conn.getChildren("/music2")));
        }
    }

    @Override
    public boolean createDir(String remoteDir) throws IOException {
        return ftpClient.makeDirectory(extract(remoteDir));
    }

    @Override
    public void copyFile(String remotePath, File localPath) throws IOException {
        try (FileInputStream local = new FileInputStream(localPath)) {
            ftpClient.storeFile(extract(remotePath), local);
        }
    }

    @Override
    public String[] getChildren(String remotDir) throws IOException {
        return ftpClient.listNames(extract(remotDir));
    }

    private String extract(String remotePath) {
        if (remotePath.startsWith(extractPath)) {
            return remotePath.substring(extractPath.length());
        } else {
            return remotePath;
        }
    }

    @Override
    public boolean dirExists(String remoteDir) throws IOException {
        return ftpClient.getModificationTime(extract(remoteDir)) != null;
    }
}
