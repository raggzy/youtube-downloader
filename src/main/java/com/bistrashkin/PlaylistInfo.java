package com.bistrashkin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raggzy on 4/23/2016.
 */
public class PlaylistInfo {
    public static class VideoInfo {
        public final String videoUrl;
        public final String videoTitle;

        public VideoInfo(String videoUrl, String videoTitle) {
            this.videoUrl = videoUrl;
            this.videoTitle = videoTitle;
        }
    }

    public final String title;
    public final List<VideoInfo> videoInfos;

    public PlaylistInfo(String title, List<VideoInfo> videoInfos) {
        this.title = title;
        this.videoInfos = videoInfos;
    }
}
