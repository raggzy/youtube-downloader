package com.bistrashkin;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by raggzy on 4/23/2016.
 */
public class Metadata {
    public enum Field {
        album,
        artist,
        title,
        year,
        genre
    }
    public final Map<Field,String> values = new HashMap<>();

    public void put(Field name, String value) {
        values.put(name, value);
    }

    public String get(Field name) {
        return values.get(name);
    }
}
