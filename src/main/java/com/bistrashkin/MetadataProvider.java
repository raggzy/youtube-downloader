package com.bistrashkin;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by raggzy on 4/23/2016.
 */
public class MetadataProvider {
    private static final SimpleDateFormat YEAR_FORMAT = new SimpleDateFormat("yyyy");
    private static final String GENRE_MARKER = "(Musical Genre)";

    public static final String UPLOADS_FROM = "Uploads from";

    private static Set<String> getArtists(String videoTitle) {
        if (videoTitle != null && videoTitle.contains("-")) {
            String left = videoTitle.substring(0, videoTitle.indexOf("-")).trim();
            return Arrays.stream(left.split("[+&,]"))
                    .map(String::trim).filter(a -> !a.isEmpty())
                    .collect(Collectors.toSet());
        }
        return Collections.emptySet();
    }

    private static Set<String> getGenres(List<String> keywords) {
        return keywords.stream()
                .map(String::trim)
                .filter(k -> !k.isEmpty() && k.endsWith(GENRE_MARKER))
                .map(k -> k.substring(0, k.length() - GENRE_MARKER.length()).trim())
                .filter(k -> !k.isEmpty())
                .collect(Collectors.toSet());
    }

    private static String getAlbum(String playlistTitle) {
        if (playlistTitle == null) return null;
        if (playlistTitle.startsWith(UPLOADS_FROM)) {
            return playlistTitle.substring(UPLOADS_FROM.length()).trim();
        }
        return playlistTitle;
    }

    public Metadata getMetadata(PlaylistInfo playlistInfo, VideoInfo videoInfo) {
        Metadata metadata = new Metadata();
        if (playlistInfo != null) {
            metadata.put(Metadata.Field.album, getAlbum(playlistInfo.title));
        }
        if (videoInfo != null) {
            metadata.put(Metadata.Field.artist, getArtists(videoInfo.title).stream().collect(Collectors.joining("/")));
            metadata.put(Metadata.Field.title, videoInfo.title);
            metadata.put(Metadata.Field.year, YEAR_FORMAT.format(videoInfo.publishDate));
            metadata.put(Metadata.Field.genre, getGenres(videoInfo.keywords).stream().collect(Collectors.joining("/")));
        }
        return metadata;
    }
}
