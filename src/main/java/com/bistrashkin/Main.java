package com.bistrashkin;

import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by raggzy on 4/23/2016.
 */
public class Main {
    private static void delete(File f) throws IOException {
        Files.delete(f.toPath());
    }

    private static TargetFileSystemConnection getFS(String path) {
        if (path.startsWith("ftp://")) {
            return new FtpFileSystemConnection(path);
        } else {
            return new LocalFileSystemConnection();
        }
    }

    public static void convertPlayList(MainOptions options) throws Exception {
        Converter converter = new FfmpegConverter(options.getFfmpegPath());
        try (Downloader downloader = new Downloader(options.getProxyHost(), options.getProxyPort());
             TargetFileSystemConnection targetFS = getFS(options.getDestPath())) {
            File tmpFolder = new File(options.getTmpFolder());
            if (!tmpFolder.isDirectory() && !tmpFolder.exists()) {
                System.out.println("Couldn't resolve tmp folder");
                System.exit(-1);
            }
            if (targetFS instanceof LocalFileSystemConnection) {
                if (new File(options.getDestPath()).getCanonicalPath().equals(tmpFolder.getCanonicalPath())) {
                    System.out.print("Dest folder couldn't be same as temp. Choose another");
                    System.exit(-1);
                }
            }
            targetFS.open();
            if (!targetFS.dirExists(options.getDestPath())) {
                if (!targetFS.createDir(options.getDestPath())) {
                    System.out.println("Couldn't create target dir. Exiting");
                    System.exit(-1);
                }
            }
            Set<String> children = new HashSet<>(Arrays.asList(targetFS.getChildren(options.getDestPath())));
            MetadataProvider metadataProvider = new MetadataProvider();
            SignatureCalculator signatureCalculator = new SignatureCalculator();
            Parser parser = new Parser(downloader, signatureCalculator);
            System.out.println("Parsing playlist");
            PlaylistInfo playlistInfo = parser.parsePlaylistUrl(options.getPlaylistUrl());
            System.out.println(String.format("Downloading %s to %s", playlistInfo.title, options.getDestPath()));
            int progress = 0;
            for (PlaylistInfo.VideoInfo playlistVideoInfo : playlistInfo.videoInfos) {
                try {
                    String filename = playlistVideoInfo.videoTitle.replaceAll("[<>:\"/|?*,\\\\]", "");
                    String filenameMp3 = filename + ".mp3";
                    File tmpDownloadFile = new File(tmpFolder, filename + ".tmp");
                    File tmpConvertFile = new File(tmpFolder, filenameMp3);
                    String destPath = options.getDestPath() + "/" + filenameMp3;
                    if (!children.contains(filenameMp3)) {
                        VideoInfo videoInfo = parser.parseVideoUrl(playlistVideoInfo.videoUrl);
                        System.out.println(String.format("Downloading %s to %s", videoInfo.title, tmpDownloadFile.getCanonicalPath()));
                        if (videoInfo.downloadUrl == null) {
                            System.out.println("No audio stream found");
                            continue;
                        }
                        byte[] bytes = downloader.downloadVideo(videoInfo);
                        FileUtils.writeByteArrayToFile(tmpDownloadFile, bytes);
                        System.out.println(String.format("Converting %s to %s", videoInfo.title, tmpConvertFile.getCanonicalPath()));
                        converter.convert(tmpDownloadFile.getCanonicalPath(), tmpConvertFile.getCanonicalPath(), metadataProvider.getMetadata(playlistInfo, videoInfo));
                        delete(tmpDownloadFile);
                        System.out.println(String.format("Copying %s to %s", videoInfo.title, destPath));
                        targetFS.copyFile(destPath, tmpConvertFile);
                        delete(tmpConvertFile);
                    }
                } catch (Exception e) {
                    System.out.println(String.format("Error while processing %s", playlistVideoInfo.videoUrl));
                    e.printStackTrace();
                } finally {
                    System.out.println(String.format("%d/%d", (++progress), playlistInfo.videoInfos.size()));
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("playlistUrl", true, "youtube.com playlist url");
        options.addOption("destPath", true, "download to destination playlist url. Ftp or local");
        options.addOption("ffmpegPath", true, "path to ffmpeg binary");
        options.addOption("proxyHost", true, "proxy host");
        options.addOption("proxyPort", true, "proxy port, int");
        options.addOption("tmpFolder", true, "temp folder");
        CommandLineParser commandLineParser = new GnuParser();
        CommandLine commandLine = commandLineParser.parse(options, args);
        MainOptions mainOptions = new MainOptions();
        try {
            mainOptions.parse(commandLine);
            mainOptions.validate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printUsage(new PrintWriter(System.out), 30, "app", options);
            System.exit(-1);
            return;
        }
        convertPlayList(mainOptions);
    }
}
