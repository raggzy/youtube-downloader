package com.bistrashkin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class LocalFileSystemConnection implements TargetFileSystemConnection {
    @Override
    public boolean dirExists(String remoteDir) {
        File file = new File(remoteDir);
        return file.isDirectory() && file.exists();
    }

    @Override
    public boolean createDir(String remoteDir) {
        return new File(remoteDir).mkdir();
    }

    @Override
    public void copyFile(String remotePath, File localPath) throws IOException {
        Files.copy(localPath.toPath(), new File(remotePath).toPath(), REPLACE_EXISTING);
    }

    @Override
    public void open() throws IOException {
        // nothing
    }

    @Override
    public void close() throws IOException {
        // nothing
    }

    @Override
    public String[] getChildren(String remotDir) {
        return new File(remotDir).list();
    }
}
