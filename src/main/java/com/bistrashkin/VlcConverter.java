package com.bistrashkin;

import java.io.*;

public class VlcConverter implements Converter {
    private static final String VLC_DEFAULT_PATH = "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe";

    private final String vlcPath;

    public VlcConverter(String vlcPath) {
        this.vlcPath = vlcPath == null ? VLC_DEFAULT_PATH : vlcPath;
    }

    public void convert(String tmpFilePath, String targetFilePath, Metadata metadata) throws Exception {
        ProcessBuilder dummy = new ProcessBuilder(
                vlcPath,
                "-I",
                "dummy",
                "--dummy-quiet",
                "--no-sout-video",
                "--no-sout-rtp-sap",
                "--no-sout-standard-sap",
                "--no-crashdump",
                "--sout-audio",
                "-vvv",
                "--sout",
                String.format("#transcode{acodec=mp3,channels=2,ab=320,samplerate=44100}:standard{access=file,mux=raw,dst=%s}", targetFilePath),
                tmpFilePath,
                "vlc://quit")
                .redirectOutput(File.createTempFile("vlc", "out"))
                .redirectError(File.createTempFile("vlc", "err"));
        dummy.start().waitFor();
    }
}
