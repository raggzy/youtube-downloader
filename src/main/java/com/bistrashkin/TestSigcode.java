package com.bistrashkin;

import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * Created by raggzy on 6/16/2016.
 */
public class TestSigcode {
    public static void main(String[] args) throws Exception {
        SignatureCalculator calculator = new SignatureCalculator();
        Downloader downloader = new Downloader();
        Parser parser = new Parser(downloader, calculator);
        String sigcode = parser.extractSigcode("https://www.youtube.com/yts/jsbin/player-vflB24EJ3/en_US/base.js");
        IOUtils.write(sigcode, new BufferedOutputStream(new FileOutputStream("testsigcode.out")));
//        String sigcode = IOUtils.toString(new BufferedInputStream(new FileInputStream("testsigcode.out")));
        System.out.println(calculator.signature("whatever", sigcode));
    }
}
