package com.bistrashkin;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by raggzy on 12/8/2014.
 */
public class Selector {
    private final String source;

    public Selector(String source) {
        this.source = source;
    }

    private int pos = 0;
    private int begin = -1;
    private int end = -1;

    private Match find(String str) {
        Pattern pattern = Pattern.compile(str);
        Matcher matcher = pattern.matcher(source);
        if (matcher.find(pos)) {
            return new Match(matcher.start(), matcher.end());
        } else {
            return null;
        }
    }

    private Selector seek(int pos) {
        this.pos = pos;
        return this;
    }

    public Selector moveTo(String str) {
        Match match = find(str);
        if (match == null) {
            throw new InvalidSelection(String.format("No [%s] in source", str));
        }
        seek(match.begin);
        return this;
    }

    public Selector moveBy(int symbols) {
        pos += symbols;
        if (pos < 0 || pos >= source.length()) {
            throw new InvalidSelection();
        }
        return this;
    }

    public Selector moveReverse(String str) {
        while (true) {
            if (pos < 0) {
                throw new InvalidSelection();
            }
            Match match = find(str);
            if (match != null && match.begin == pos) {
                break;
            }
            pos--;
        }
        return this;
    }

    public Selector markBegin() {
        this.begin = pos;
        return this;
    }

    public Selector markEnd() {
        this.end = pos;
        return this;
    }

    public List<String> between(String start, String end) {
        List<String> result = new ArrayList<String>();
        while (true) {
            Match match = find(start);
            if (match == null) {
                break;
            }
            startAfter(start);
            endBefore(end);
            result.add(getSelection());
        }
        return result;
    }

    public Selector startAfter(String str) {
        Match match = find(str);
        if (match == null) {
            throw new InvalidSelection(String.format("No [%s] in source", str));
        }
        return seek(match.end).markBegin();
    }

    public Selector endBefore(String str) {
        Match match = find(str);
        if (match == null) {
            throw new InvalidSelection(String.format("No [%s] in source", str));
        }
        return seek(match.begin).markEnd();
    }

    public String getSelection() {
        return source.substring(begin, end);
    }

    public Selector crop() {
        return new Selector(getSelection());
    }

    private static class Match {
        private final int begin;
        private final int end;

        public Match(int begin, int end) {
            this.begin = begin;
            this.end = end;
        }
    }
}
