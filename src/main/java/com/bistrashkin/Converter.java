package com.bistrashkin;

import java.util.Map;

/**
 * Created by raggzy on 4/23/2016.
 */
public interface Converter {
    void convert(String tmpFilePath, String targetFilePath, Metadata metadata) throws Exception;
}
