package com.bistrashkin;

/**
 * Created by raggzy on 5/6/2016.
 */
public class InvalidSelection extends RuntimeException {

    public InvalidSelection() {
    }

    public InvalidSelection(String message) {
        super(message);
    }

    public InvalidSelection(String message, Throwable cause) {
        super(message, cause);
    }
}
