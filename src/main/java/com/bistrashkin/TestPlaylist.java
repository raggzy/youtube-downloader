package com.bistrashkin;

import java.io.IOException;

/**
 * Created by raggzy on 4/24/2016.
 */
public class TestPlaylist {
    public static void main(String[] args) {
        String url = "https://www.youtube.com/playlist?list=PLhLipSdUtJ95RCMk3am399aYmJLVSVpN5";
        try (Downloader downloader = new Downloader()) {
            SignatureCalculator signatureCalculator = new SignatureCalculator();
            Parser parser = new Parser(downloader, signatureCalculator);
            PlaylistInfo playlistInfo = parser.parsePlaylistUrl(url);
            System.out.println(playlistInfo.title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
