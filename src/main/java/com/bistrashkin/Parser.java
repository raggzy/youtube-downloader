package com.bistrashkin;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;

import java.net.URL;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by raggzy on 4/23/2016.
 */
public class Parser {
    private static final String YOUTUBE_BASE = "https://www.youtube.com";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private final Downloader downloader;
    private final SignatureCalculator signatureCalculator;
    private final Retrier retrier = new Retrier(3, InvalidSelection.class);

    public Parser(Downloader downloader, SignatureCalculator signatureCalculator) {
        this.downloader = downloader;
        this.signatureCalculator = signatureCalculator;
    }

    public String extractSigcode(String scriptUrl) throws Exception {
        String scriptContent = downloader.getStringContent(scriptUrl);
        String sigCode = new Selector(scriptContent).startAfter("var window=this;").markBegin()
                .moveTo("\"yt.player.Application.create\"").moveReverse(";").moveBy(1).markEnd()
                .getSelection();
        Pattern p = Pattern.compile("([$a-zA-Z]+)\\.set\\(([$a-zA-Z]+)\\.sp,encodeURIComponent\\(([^(]+)\\(");
        Matcher matcher = p.matcher(scriptContent);
        if (!matcher.find()) {
            throw new Exception("Invalid sigextraction, please modify me");
        }
        String sigFunctionName = matcher.group(3);
        String paramNames = new Selector(scriptContent).startAfter(";\\(function\\(").endBefore("\\)").getSelection();
        return String.format(IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("sigcode.js")), paramNames, sigCode, sigFunctionName);
    }

    private static int parseClen(String url) {
        int clen = -1;
        String clenToken = "clen=";
        int clenStart = url.indexOf(clenToken);
        if (clenStart >= 0) {
            int clenEnd = url.indexOf("&", clenStart);
            clen = Integer.valueOf(url.substring(clenStart + clenToken.length(), clenEnd == -1 ? url.length() : clenEnd));
        }
        return clen;
    }

    private List<Map<String, String>> parseUrlEncoded(String urlEncodedStr) {
        List<Map<String, String>> results = new ArrayList<>();
        for (String part : urlEncodedStr.split(",")) {
            Map<String, String> partMap = new HashMap<>();
            for (String keyValue : part.split("&")) {
                int idx = keyValue.indexOf("=");
                String key = keyValue.substring(0, idx);
                String value = URLDecoder.decode(keyValue.substring(idx + 1, keyValue.length()));
                partMap.put(key, value);
            }
            results.add(partMap);
        }
        return results;
    }

    public VideoInfo parseVideoUrl(String watchUrl) throws Exception {
        return retrier.run(() -> {
            String videoContent = StringEscapeUtils.unescapeHtml(downloader.getStringContent(watchUrl));
            // getting JSON
            String videoInfo = new Selector(videoContent).moveTo("\"args\":\\{").moveBy(7).markBegin().endBefore("</script>").markEnd().getSelection();
            ObjectMapper mapper = new ObjectMapper();
            @SuppressWarnings("unchecked")
            Map<String, Object> videoInfoMap = mapper.readValue(videoInfo, Map.class);
            String title = (String) videoInfoMap.get("title");
            Date publishDate = null;
            try {
                publishDate = DATE_FORMAT.parse(new Selector(videoContent).startAfter("<meta itemprop=\"datePublished\"").startAfter("content=\"").endBefore("\"").getSelection());
            } catch (ParseException ignore) {
                // ok ok, no date, no problems
            }
            String playerResponse = (String) videoInfoMap.get("player_response");
            Map playerResponseMap = mapper.readValue(playerResponse, Map.class);
            @SuppressWarnings("unchecked")
            List<String> keywords = (List<String>) ((Map) playerResponseMap.get("videoDetails")).get("keywords");
            // finding max audio type
            List<Map<String, String>> fmts = parseUrlEncoded((String) videoInfoMap.get("url_encoded_fmt_stream_map"));
            if (fmts.size() == 0) {
                throw new RuntimeException("No video format detected");
            }
            Map<String, String> bestFmt = fmts.get(0);
            String url = bestFmt.get("url");
            String s = bestFmt.get("s");
            String sp = bestFmt.get("sp");
            if (s != null && sp != null) {
                // extracting sigcode
                String relativeUrl = new Selector(videoContent).moveTo("name=\"player(_\\w+){0,1}/base\"").moveReverse("<script").startAfter("src=\"").endBefore("\"").getSelection();
                URL scriptUrl = new URL(new URL(YOUTUBE_BASE), relativeUrl);
                String sigcode = extractSigcode(scriptUrl.toString());
                url += "&" + sp + "=" + signatureCalculator.signature(s, sigcode);
            }
            return new VideoInfo(title, url, parseClen(url), keywords, publishDate);
        });
    }

    public PlaylistInfo parsePlaylistUrl(String playlistUrl) throws Exception {
        return retrier.run(() -> {
            String content = StringEscapeUtils.unescapeHtml(downloader.getStringContent(playlistUrl));
            String title = new Selector(content).moveTo("pl-header-title").startAfter(">").endBefore("</h1").getSelection().trim();
            List<PlaylistInfo.VideoInfo> videoInfos = new ArrayList<>();
            for (String str : new Selector(content).moveTo("pl-video-table").startAfter("<tbody").endBefore("</tbody>").crop().between("<tr", "</tr>")) {
                Selector eachSelector = new Selector(str);
                String htmlURL = eachSelector.moveTo("<a").startAfter("href=\"").endBefore("&").getSelection();
                String videoTitle = eachSelector.moveTo("pl-video-title-link").startAfter(">").endBefore("</a>").getSelection().trim();
                String videoUrl = String.format("%s%s", YOUTUBE_BASE, htmlURL);
                videoInfos.add(new PlaylistInfo.VideoInfo(videoUrl, videoTitle));
            }
            return new PlaylistInfo(title, videoInfos);
        });
    }
}
