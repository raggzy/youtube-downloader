package com.bistrashkin;

import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * Created by raggzy on 4/23/2016.
 */
public class TestVideo {
    public static void main(String[] args) {
        String url = "https://www.youtube.com/watch?v=yVdw02AmP2M";
        String tmpPath = "C:\\Temp\\tmp.tmp";
        String mp3Path = "C:\\Temp\\tmp.mp3";
        try (Downloader downloader = new Downloader()) {
            SignatureCalculator signatureCalculator = new SignatureCalculator();
            Parser parser = new Parser(downloader, signatureCalculator);
            Converter converter = new FfmpegConverter(MainOptions.FFMPEG_PATH_DEFAULT);
            MetadataProvider metadataProvider = new MetadataProvider();
            VideoInfo videoInfo = parser.parseVideoUrl(url);
            FileUtils.writeByteArrayToFile(new File(tmpPath), downloader.downloadVideo(videoInfo));
            converter.convert(tmpPath, mp3Path, metadataProvider.getMetadata(null, videoInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
