package com.bistrashkin;

import org.apache.commons.io.IOUtils;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by raggzy on 4/23/2016.
 */
public class Downloader implements AutoCloseable {
    private static final int PORTION = 512 * 1024;
    private static final int MAX_THREADS = 8;
    private static final int SLEEP_AFTER_MS = 1000;
    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(MAX_THREADS, MAX_THREADS, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
    private Proxy proxy;
    private Retrier retrier = new Retrier(3);

    public Downloader() {
        proxy = Proxy.NO_PROXY;
    }

    public Downloader(String proxyHost, int proxyPort) throws NoSuchAlgorithmException, KeyManagementException {
        this();
        if (proxyHost != null && !proxyHost.isEmpty()) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        }
    }

    private static SSLSocketFactory trustingFactory() throws Exception {
        SSLContext context = SSLContext.getInstance("SSL");
        context.init(new KeyManager[]{}, new TrustManager[]{new X509TrustManager() {
            @Override
            public synchronized void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public synchronized void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }}, new SecureRandom());
        return context.getSocketFactory();
    }

    private URLConnection getUrlConnection(String url) throws Exception {
        URLConnection urlConnection = new URL(url).openConnection(proxy);
        if (urlConnection instanceof HttpsURLConnection) {
            HttpsURLConnection httpsConnection = (HttpsURLConnection) urlConnection;
            httpsConnection.setSSLSocketFactory(trustingFactory());
        }
        urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36");
        urlConnection.setRequestProperty("Accept-Language", "en");
        urlConnection.setConnectTimeout(1000);
        urlConnection.setReadTimeout(5000);
        return urlConnection;
    }

    public String getStringContent(String url) throws Exception {
        return retrier.run(() -> {
            URLConnection urlConnection = getUrlConnection(url);
            urlConnection.setRequestProperty("Accept-Charset", "utf-8");
            urlConnection.connect();
            return IOUtils.toString(urlConnection.getInputStream(), "utf-8");
        });
    }

    public byte[] getByteContent(String url) throws Exception {
        return retrier.run(() -> {
            URLConnection urlConnection = getUrlConnection(url);
            urlConnection.connect();
            return IOUtils.toByteArray(urlConnection.getInputStream());
        });
    }

    public byte[] downloadVideo(VideoInfo videoInfo) throws Exception {
        if (videoInfo.clen <= 0) {
            return retrier.run(() -> getByteContent(videoInfo.downloadUrl));
        }
        byte[] res = new byte[videoInfo.clen];
        int requestsCount = (int) Math.ceil(1.0 * videoInfo.clen / PORTION);
        CountDownLatch done = new CountDownLatch(requestsCount);
        AtomicInteger errors = new AtomicInteger(0);
        AtomicInteger rn = new AtomicInteger(0);
        for (int count = 0; count < requestsCount; count++) {
            int from = count * PORTION;
            int len = Math.min(videoInfo.clen, (count + 1) * PORTION) - from;
            executor.execute(() -> {
                try {
                    retrier.run(() -> {
                        if (errors.get() > 0) return null;
                        byte[] readBytes = getByteContent(videoInfo.downloadUrl + "&range=" + from + "-" + (from + len - 1) + "&rn=" + rn.incrementAndGet());
                        System.arraycopy(readBytes, 0, res, from, readBytes.length);
                        Thread.sleep(SLEEP_AFTER_MS);
                        return null;
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    errors.incrementAndGet();
                } finally {
                    done.countDown();
                }
            });
        }
        done.await();
        if (errors.get() > 0) {
            throw new IOException("IO error while download");
        }
        return res;
    }

    @Override
    public void close() {
        shutdown();
    }

    public void shutdown() {
        executor.shutdown();
    }
}
