package com.bistrashkin;

import java.io.File;
import java.io.IOException;

public interface TargetFileSystemConnection extends AutoCloseable {
    void open() throws IOException;

    void close() throws IOException;

    boolean dirExists(String remoteDir) throws IOException;

    boolean createDir(String remoteDir) throws IOException;

    void copyFile(String remotePath, File localPath) throws IOException;

    String[] getChildren(String remotDir) throws IOException;
}
