package com.bistrashkin;

public class Retrier {
    private final int retries;
    private final Class<? extends Exception>[] allowedExceptions;

    public Retrier(int retries) {
        this(retries, Exception.class);
    }

    public Retrier(int retries, Class<? extends Exception>... allowedExceptions) {
        this.retries = retries;
        this.allowedExceptions = allowedExceptions;
    }

    public <T> T run(Action<T> action) throws Exception {
        Exception lastException = null;
        for (int i = 0; i < retries; i++) {
            try {
                return action.perform();
            } catch (Exception e) {
                boolean matched = false;
                for (Class<? extends Exception> exception : allowedExceptions) {
                    if (exception.isAssignableFrom(e.getClass())) {
                        matched = true;
                        break;
                    }
                }
                if (matched) {
                    lastException = e;
                } else {
                    throw e;
                }
            }
        }
        throw lastException;
    }

    public static interface Action<T> {
        T perform() throws Exception;
    }
}
