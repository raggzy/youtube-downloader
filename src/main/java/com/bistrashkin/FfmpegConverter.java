package com.bistrashkin;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.*;

public class FfmpegConverter implements Converter {
    private static final Map<String, Metadata.Field> SUPPORTED_METADATA = new HashMap<String, Metadata.Field>() {{
        put("album", Metadata.Field.album);
        put("artist", Metadata.Field.artist);
        put("title", Metadata.Field.title);
        put("TYER", Metadata.Field.year);
        put("genre", Metadata.Field.genre);
    }};

    private final String ffmpegPath;

    public FfmpegConverter(String ffmpegPath) {
        this.ffmpegPath = ffmpegPath;
    }

    private static String escapeMetaValue(String fieldValue) {
        return fieldValue.replaceAll("([=;#\\\\])", "\\\\$1");
    }

    public void convert(String tmpFilePath, String targetFilePath, Metadata metadata) throws Exception {
        List<String> command = new LinkedList<>();
        command.add(ffmpegPath);
        command.add("-i");
        command.add(tmpFilePath);
        // meta
        File metaFile = File.createTempFile("ffmpeg", "metadata");
        List<String> metaLines = new ArrayList<>();
        metaLines.add(";FFMETADATA1");
        // metadata
        for (Map.Entry<String, Metadata.Field> entry : SUPPORTED_METADATA.entrySet()) {
            String fieldName = entry.getKey();
            String fieldValue = metadata.get(entry.getValue());
            if (fieldValue != null && !fieldValue.isEmpty()) {
                metaLines.add(fieldName + "=" + escapeMetaValue(fieldValue));
            }
        }
        try (FileOutputStream output = new FileOutputStream(metaFile)) {
            IOUtils.writeLines(metaLines, System.getProperty("line.separator"), output, "utf8");
        }
        command.add("-i");
        command.add(metaFile.getCanonicalPath());
        command.add("-map_metadata");
        command.add("1");
        // quality
        command.add("-codec:a");
        command.add("libmp3lame");
        command.add("-q:a");
        command.add("0");
        command.add("-id3v2_version");
        command.add("3");
        command.add("-write_id3v1");
        command.add("0");
        command.add("-y");
        command.add(targetFilePath);
        File out = File.createTempFile("ffmpeg", "out");
        File err = File.createTempFile("ffmpeg", "err");
        ProcessBuilder dummy = new ProcessBuilder(command)
                .redirectOutput(out)
                .redirectError(err);
        if (dummy.start().waitFor() != 0) {
            throw new RuntimeException(command.toString() + " returned " + IOUtils.toString(new FileInputStream(err)));
        }
    }
}
