package com.bistrashkin;

import java.util.Date;
import java.util.List;

/**
 * Created by raggzy on 4/23/2016.
 */
public class VideoInfo {
    public final String downloadUrl;
    public final String title;
    public final int clen;
    public final List<String> keywords;
    public final Date publishDate;

    public VideoInfo(String title, String downloadUrl, int clen, List<String> keywords, Date publishDate) {
        this.title = title;
        this.downloadUrl = downloadUrl;
        this.clen = clen;
        this.keywords = keywords;
        this.publishDate = publishDate;
    }
}
