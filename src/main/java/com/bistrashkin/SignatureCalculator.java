package com.bistrashkin;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by raggzy on 6/5/2016.
 */
public class SignatureCalculator {
    private Map<String, ScriptEngine> sigcodeExecutors = new HashMap<>();

    public String signature(String s, String sigcode) {
        try {
            if (!sigcodeExecutors.containsKey(sigcode)) {
                ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
                engine.eval(sigcode);
                sigcodeExecutors.put(sigcode, engine);
            }
            return (String) sigcodeExecutors.get(sigcode).eval(String.format("signature(\"%s\")", s));
        } catch (Exception e) {
            throw new RuntimeException("Unable to execute sigcode", e);
        }
    }
}
